#!/bin/bash
#.make.sh
# this script create symlinks from ~/ to dotfiles dir

dir=~/dotfiles
olddir=~/dotfiles_old
files=".bashrc .vimrc .zshrc .gitconfig"

echo "creating $olddir for backup of existing dotfiles in ~"
mkdir p $olddir
echo "...complete..."

echo "changing to the $dir"
cd $dir
echo "..complete.."

for file in $files; do
  echo "moving existing dotfiles in ~ to $olddir"
  mv ~/$file $olddir/
  echo "creating symlink to $file in ~ repo"
  ln -s $dir/$file ~/$file
done
